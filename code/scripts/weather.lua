#!/usr/bin/lua
http = require("socket.http")
json = require("json")

unit = "metric"
measure = '°' .. (unit == 'metric' and 'C' or 'F')

apikey = "85a4e3c55b73909f42c6a23ec35b7147"
id = "7290254"

icons = {
  ["01"] = "",
  ["02"] = "",
  ["03"] = "",
  ["04"] = "",
  ["09"] = "",
  ["10"] = "",
  ["11"] = "",
  ["13"] = "",
  ["50"] = "",
}

weather = json.decode(http.request( "http://api.openweathermap.org/data/2.5/weather?id=" .. id .. "&units=" .. unit .. "&APPID=" .. apikey))

temp = weather.main.temp
conditions = weather.weather[1].main
icon = weather.weather[1].icon:sub(1, 2)

print(icons[icon] .. "  " .. temp .. measure .. ", " .. conditions)
